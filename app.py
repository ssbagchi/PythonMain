
from flask import Flask , render_template , request, json
from flaskext.mysql import MySQL
from datetime import datetime

#from werkzeug import generate_password_hash, check_password_hash 

import re


app = Flask(__name__)

mysql = MySQL()
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'ssb'
app.config['MYSQL_DATABASE_PASSWORD'] = 'ssb@123'
app.config['MYSQL_DATABASE_DB'] = 'Test'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/contact")
def contact():
    return render_template("contact.html")

@app.route("/hello/<name>")
def hello_there(name):
    return render_template(
        "hello.html",
        name=name,
        date=datetime.now())

@app.route("/showsignup")
def showSignUp():
    return render_template("signup.html")

@app.route('/signUp',methods=['POST'])
def signUp():
    try: 
       _name = request.form['inputName']
       _email = request.form['inputEmail']
       _password = request.form['inputPassword']

    #if _name and _email and _password:
    #    return json.dumps({'html':'<span>All fields good !!</span>'})
    #else:
    #    return json.dumps({'html':'<span>Enter the required fields</span>'})
    
       conn = mysql.connect()
       cursor = conn.cursor()
       cursor.callproc("sp_createUser",(_name,_email,_password))
       data = cursor.fetchall()
 
       if len(data) is 0:
          conn.commit()
          return json.dumps({'message':'User created successfully !'})
       else:
          return json.dumps({'error':str(data[0])}) 
    except Exception as e:
        return reder_template('error.html', error = str(e))
    finally:
        cursor.close()
        conn.close()
        
